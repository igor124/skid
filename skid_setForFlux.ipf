#pragma rtGlobals=3		// Use modern global access method and strict wave access.



_______________________________________________________________________________________________

												Link setFormula pour le flux (vanne)
_______________________________________________________________________________________________



function skid_setFor_mt204vanne(Var_mt204vanne)
	variable Var_mt204vanne

	NVAR mt204vanne = root:SKID:Variables:mt204vanne
	NVAR CPT_mt204vanne = root:SKID:Variables:CPT_mt204vanne
	WAVE Wave_mt204vanne = root:SKID:Variables:Wave_mt204vanne
	NVAR t0 = root:SKID:Variables:t0
	
	CPT_mt204vanne += 1
	redimension /N=(CPT_mt204vanne, 2) Wave_mt204vanne
	Wave_mt204vanne[CPT_mt204vanne - 1][0] = (dateTime - t0)/60		// en minutes
	Wave_mt204vanne[CPT_mt204vanne - 1][1] = mt204vanne

	return 0
end