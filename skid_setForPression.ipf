#pragma rtGlobals=3		// Use modern global access method and strict wave access.



_______________________________________________________________________________________________

												Links setFormula Pour les pressions
_______________________________________________________________________________________________




function skid_setFor_mp201d(Var_mp201d)
	variable Var_mp201d

	NVAR mp201d = root:SKID:Variables:mp201d
	NVAR CPT_mp201d = root:SKID:Variables:CPT_mp201d
	WAVE Wave_mp201d = root:SKID:Variables:Wave_mp201d
	NVAR t0 = root:SKID:Variables:t0
	
	
	// Point de v�rif si on a pas d�pass� le temps total si checkbox 
	// checkFTpsMonitoring coch�e
	// _______________________________________________________________
	
	controlInfo /W=skidPanel checkFTpsMonitoring
	if(V_Value == 1)
	
		NVAR tpsTotMonit = root:SKID:Variables:tpsTotMonit
		
		if (dateTime - t0 > tpsTotMonit*3600)

			Button buttonSkidGo win = skidPanel, disable = 0
			Button buttonSkidStop win = skidPanel, disable = 2
			CheckBox checkFTpsMonitoring win = skidPanel, disable = 0
			SetVariable setvarSkidTpsMonitoring win = skidPanel, disable = 0
			Button buttonSkidFermer win = skidPanel, disable = 0
			
			skid_stopMonitoring()			// Arr�t monitorage
			skid_setFormulaOFF()			// Rupture liaisons d�pendances
			
			sauvegardeDonnees()				// sauvegarde des waves au format : nomWave - Date - Heure.txt			
			
			doAlert /T= "Monitorage" 0, "FIN monitorage"
			
			return 0
		endif
	endif 
	// _______________________________________________________________
	
	
	
	CPT_mp201d += 1
	redimension /N=(CPT_mp201d, 2) Wave_mp201d
	Wave_mp201d[CPT_mp201d - 1][0] = (dateTime - t0)/60		// en minutes
	Wave_mp201d[CPT_mp201d - 1][1] = mp201d

	return 0
end



function skid_setFor_mp202d(Var_mp202d)
	variable Var_mp202d

	NVAR mp202d = root:SKID:Variables:mp202d
	NVAR CPT_mp202d = root:SKID:Variables:CPT_mp202d
	WAVE Wave_mp202d = root:SKID:Variables:Wave_mp202d
	NVAR t0 = root:SKID:Variables:t0
	
	CPT_mp202d += 1
	redimension /N=(CPT_mp202d, 2) Wave_mp202d
	Wave_mp202d[CPT_mp202d - 1][0] = (dateTime - t0)/60		// en minutes
	Wave_mp202d[CPT_mp202d - 1][1] = mp202d

	return 0
end


function skid_setFor_mp205d(Var_mp205d)
	variable Var_mp205d

	NVAR mp205d = root:SKID:Variables:mp205d
	NVAR CPT_mp205d = root:SKID:Variables:CPT_mp205d
	WAVE Wave_mp205d = root:SKID:Variables:Wave_mp205d
	NVAR t0 = root:SKID:Variables:t0
	
	CPT_mp205d += 1
	redimension /N=(CPT_mp205d, 2) Wave_mp205d
	Wave_mp205d[CPT_mp205d - 1][0] = (dateTime - t0)/60		// en minutes
	Wave_mp205d[CPT_mp205d - 1][1] = mp205d

	return 0
end



function skid_setFor_mp203r(Var_mp203r)
	variable Var_mp203r

	NVAR mp203r = root:SKID:Variables:mp203r
	NVAR CPT_mp203r = root:SKID:Variables:CPT_mp203r
	WAVE Wave_mp203r = root:SKID:Variables:Wave_mp203r
	NVAR t0 = root:SKID:Variables:t0
	
	CPT_mp203r += 1
	redimension /N=(CPT_mp203r, 2) Wave_mp203r
	Wave_mp203r[CPT_mp203r - 1][0] = (dateTime - t0)/60		// en minutes
	Wave_mp203r[CPT_mp203r - 1][1] = mp203r

	return 0
end



function skid_setFor_mp204r(Var_mp204r)
	variable Var_mp204r

	NVAR mp204r = root:SKID:Variables:mp204r
	NVAR CPT_mp204r = root:SKID:Variables:CPT_mp204r
	WAVE Wave_mp204r = root:SKID:Variables:Wave_mp204r
	NVAR t0 = root:SKID:Variables:t0
	
	CPT_mp204r += 1
	redimension /N=(CPT_mp204r, 2) Wave_mp204r
	Wave_mp204r[CPT_mp204r - 1][0] = (dateTime - t0)/60		// en minutes
	Wave_mp204r[CPT_mp204r - 1][1] = mp204r


	return 0
end




function skid_setFor_mp206r(Var_mp206r)
	variable Var_mp206r

	NVAR mp206r = root:SKID:Variables:mp206r
	NVAR CPT_mp206r = root:SKID:Variables:CPT_mp206r
	WAVE Wave_mp206r = root:SKID:Variables:Wave_mp206r
	NVAR t0 = root:SKID:Variables:t0
	
	CPT_mp206r += 1
	redimension /N=(CPT_mp206r, 2) Wave_mp206r
	Wave_mp206r[CPT_mp206r - 1][0] = (dateTime - t0)/60		// en minutes
	Wave_mp206r[CPT_mp206r - 1][1] = mp206r

	return 0
end



function skid_setFor_mp207r(Var_mp207r)
	variable Var_mp207r

	NVAR mp207r = root:SKID:Variables:mp207r
	NVAR CPT_mp207r = root:SKID:Variables:CPT_mp207r
	WAVE Wave_mp207r = root:SKID:Variables:Wave_mp207r
	NVAR t0 = root:SKID:Variables:t0
	
	CPT_mp207r += 1
	redimension /N=(CPT_mp207r, 2) Wave_mp207r
	Wave_mp207r[CPT_mp207r - 1][0] = (dateTime - t0)/60		// en minutes
	Wave_mp207r[CPT_mp207r - 1][1] = mp207r

	return 0
end