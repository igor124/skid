#pragma rtGlobals=3		// Use modern global access method and strict wave access.



_______________________________________________________________________________________________

												Links setFormula pour les T�emp�ratures
_______________________________________________________________________________________________




function skid_setFor_mt201d(Var_mt201d)
	variable Var_mt201d

	NVAR mt201d = root:SKID:Variables:mt201d
	NVAR CPT_mt201d = root:SKID:Variables:CPT_mt201d
	WAVE Wave_mt201d = root:SKID:Variables:Wave_mt201d
	NVAR t0 = root:SKID:Variables:t0
	
	CPT_mt201d += 1
	redimension /N=(CPT_mt201d, 2) Wave_mt201d
	Wave_mt201d[CPT_mt201d - 1][0] = (dateTime - t0)/60		// en minutes
	Wave_mt201d[CPT_mt201d - 1][1] = mt201d

	return 0
end



function skid_setFor_mt203d(Var_mt203d)
	variable Var_mt203d
	
	NVAR mt203d = root:SKID:Variables:mt203d
	NVAR CPT_mt203d = root:SKID:Variables:CPT_mt203d
	WAVE Wave_mt203d = root:SKID:Variables:Wave_mt203d
	NVAR t0 = root:SKID:Variables:t0
	
	CPT_mt203d += 1
	redimension /N=(CPT_mt203d, 2) Wave_mt203d
	Wave_mt203d[CPT_mt203d - 1][0] = (dateTime - t0)/60		// en minutes
	Wave_mt203d[CPT_mt203d - 1][1] = mt203d	
	
	return 0
end


function skid_setFor_mt205d(Var_mt205d)
	variable Var_mt205d
	
	NVAR mt205d = root:SKID:Variables:mt205d
	NVAR CPT_mt205d = root:SKID:Variables:CPT_mt205d
	WAVE Wave_mt205d = root:SKID:Variables:Wave_mt205d
	NVAR t0 = root:SKID:Variables:t0
	
	CPT_mt205d += 1
	redimension /N=(CPT_mt205d, 2) Wave_mt205d
	Wave_mt205d[CPT_mt205d - 1][0] = (dateTime - t0)/60		// en minutes
	Wave_mt205d[CPT_mt205d - 1][1] = mt205d		
	
	return 0
end



function skid_setFor_mt202r(Var_mt202r)
	variable Var_mt202r
	
	NVAR mt202r = root:SKID:Variables:mt202r
	NVAR CPT_mt202r = root:SKID:Variables:CPT_mt202r
	WAVE Wave_mt202r = root:SKID:Variables:Wave_mt202r
	NVAR t0 = root:SKID:Variables:t0
	
	CPT_mt202r += 1
	redimension /N=(CPT_mt202r, 2) Wave_mt202r
	Wave_mt202r[CPT_mt202r - 1][0] = (dateTime - t0)/60		// en minutes
	Wave_mt202r[CPT_mt202r - 1][1] = mt202r			
	
	return 0
end



function skid_setFor_mt204r(Var_mt204r)
	variable Var_mt204r
	
	NVAR mt204r = root:SKID:Variables:mt204r
	NVAR CPT_mt204r = root:SKID:Variables:CPT_mt204r
	WAVE Wave_mt204r = root:SKID:Variables:Wave_mt204r
	NVAR t0 = root:SKID:Variables:t0
	
	CPT_mt204r += 1
	redimension /N=(CPT_mt204r, 2) Wave_mt204r
	Wave_mt204r[CPT_mt204r - 1][0] = (dateTime - t0)/60		// en minutes
	Wave_mt204r[CPT_mt204r - 1][1] = mt204r		
	
	return 0
end



function skid_setFor_mt206r(Var_mt206r)
	variable Var_mt206r
	
	NVAR mt206r = root:SKID:Variables:mt206r
	NVAR CPT_mt206r = root:SKID:Variables:CPT_mt206r
	WAVE Wave_mt206r = root:SKID:Variables:Wave_mt206r
	NVAR t0 = root:SKID:Variables:t0
	
	CPT_mt206r += 1
	redimension /N=(CPT_mt206r, 2) Wave_mt206r
	Wave_mt206r[CPT_mt206r - 1][0] = (dateTime - t0)/60		// en minutes
	Wave_mt206r[CPT_mt206r - 1][1] = mt206r			
	
	return 0
end