#pragma rtGlobals=3		// Use modern global access method and strict wave access.


____________________ Adresses tango T�, P, Flux __________________

Pression
sli-skid/gtc/mp201-d		sli-skid/gtc/mp203-r
sli-skid/gtc/mp202-d		sli-skid/gtc/mp204-r
sli-skid/gtc/mp205-d		sli-skid/gtc/mp206-r
										sli-skid/gtc/mp207-r


T�emp�rature
sli-skid/gtc/mt201-d		sli-skid/gtc/mt202-r
sli-skid/gtc/mt203-d		sli-skid/gtc/mt204-r
sli-skid/gtc/mt205-d		sli-skid/gtc/mt206-r


Vanne
sli-skid/gtc/mt204-vanne
__________________________________________________________________




// Variables programme
function skid_variables()

	string previous_df = getdatafolder(1)
		
		
		setDataFolder root:SKID:Variables
		
		
		// Path & File
			WAVE /T skidPathNfile
			String /G skidPath ; skidPath = skidPathNfile[0]
			String /G skidFile ; skidFile = skidPathNfile[1]		// En l'�tat ne sert � rien
			
		// Temps total du monitoring
			Variable /G tpsTotMonit
		
		// D�part chrono monitorage_________________ 
			// Qd variable (T�, P, Flux) m�j son temps est (dateTime - t0)
			Variable /G t0	
			
		// Mode de fonctionnement : monitoring | background | fake
			String /G mode = "monitoring"		  
			Variable /G compteur
		
		
		
//Pressions________________________________________
			// Variables monitor�es pour les pressions
			Variable /G mp201d, mp202d, mp205d
			Variable /G mp203r, mp204r, mp206r, mp207r
			// Variables li�es pour SetFormula (SF_)
			Variable /G SF_mp201d, SF_mp202d, SF_mp205d
			Variable /G SF_mp203r, SF_mp204r, SF_mp206r, SF_mp207r
			// Variables compteur (CPT_) pour MAJ des Waves de r�ception des pressions monitor�es
			Variable /G CPT_mp201d, CPT_mp202d, CPT_mp205d
			Variable /G CPT_mp203r, CPT_mp204r, CPT_mp206r, CPT_mp207r						
			// Waves de r�ception des pressions monitor�es : 
			// 1er colonne = (dateTime - t0), 2eme colonne = la pression monitor�e au moment de sa m�j
			Make /O /N=(0, 2) Wave_mp201d, Wave_mp202d, Wave_mp205d
			Make /O /N=(0, 2) Wave_mp203r, Wave_mp204r, Wave_mp206r, Wave_mp207r	
			
		
		
// T�emp�ratures__________________________________
			// Variables monitor�es pour les T�
			Variable /G mt201d, mt203d, mt205d  
			Variable /G mt202r, mt204r, mt206r
			// Variables li�es pour SetFormula (SF_)
			Variable /G SF_mt201d, SF_mt203d, SF_mt205d  
			Variable /G SF_mt202r, SF_mt204r, SF_mt206r
			// Variables compteur (CPT_) pour MAJ des Waves de r�ception des T� monitor�es
			Variable /G CPT_mt201d, CPT_mt203d, CPT_mt205d  
			Variable /G CPT_mt202r, CPT_mt204r, CPT_mt206r 
			// Waves de r�ception des T� monitor�es
			Make /O /N=(0, 2) Wave_mt201d, Wave_mt203d, Wave_mt205d
			Make /O /N=(0, 2) Wave_mt202r, Wave_mt204r, Wave_mt206r
			
			
			
// Vanne (r�gulation flux T�)____________________
			Variable /G mt204vanne
			// Variable li�e pour SetFormula (SF_)
			Variable /G SF_mt204vanne
			// Variable compteur (CPT_) pour MAJ de la Wave de r�ception flux T�
			Variable /G CPT_mt204vanne
			// Waves de r�ception des flux de T�
			Make /O /N=(0, 2) Wave_mt204vanne
	
	
	
			
			
		// Etat bouton Go/Resume
			String /G goResume = "Go"		// Initiais� � Go
			
		// Liste des adresses des devices
			String /G adrPressureList = "sli-skid/gtc/mp201-d;sli-skid/gtc/mp202-d;sli-skid/gtc/mp205-d;sli-skid/gtc/mp203-r;sli-skid/gtc/mp204-r;sli-skid/gtc/mp206-r;sli-skid/gtc/mp207-r;"
			String /G adrTemperatureList = "sli-skid/gtc/mt201-d;sli-skid/gtc/mt203-d;sli-skid/gtc/mt205-d;sli-skid/gtc/mt202-r;sli-skid/gtc/mt204-r;sli-skid/gtc/mt206-r;"
				
			
		// Liste des variables recueillant les valeurs (P, T�, Flux)	
			String /G varPressure  = "mp201d;mp202d;mp205d;mp203r;mp204r;mp206r;mp207r;"
			String /G varTemperature  = "mt201d;mt203d;mt205d;mt202r;mt204r;mt206r;"
			
			
			
			
			//sli-skid/gtc/mt204-vanne;
			//mt204vanne;
			
			

		setDataFolder previous_df
		
	return 0
end





