#pragma rtGlobals=3		// Use modern global access method and strict wave access.

					________________________________________________
	
					 								IHM 
					 Control panel, Pressions, T�emp�ratures, Flux
					________________________________________________



function skid_controlPanel()

	doWindow /F skidPanel
	if(V_flag == 1)
	
		return 0
	endif

	PauseUpdate; Silent 1		// building window...
	NewPanel /N=skidPanel /K=2 /W=(977,647,1614,751)
	ModifyPanel /W=skidPanel fixedSize = 1
	
	SetVariable setvarSkidPath,pos={15,11},size={600,19},proc=SetVarProc_skidPath,title="Path"
	SetVariable setvarSkidPath,fSize=12,limits={-inf,inf,0}, value = root:SKID:Variables:skidPath
	
	Button buttonSkidGo,pos={151,50},size={30,30},proc=ButtonProc_skidGo,title="\\W649"	// "\\W648" = blanc
	Button buttonSkidStop,pos={114,50},size={30,30},disable = 2,proc=ButtonProc_skidStop,title="\\W605"		// "\\W616" noir
	
	CheckBox checkFTpsMonitoring,pos={307,56},size={176,16}, title="Temps monitoring (heures)"
	CheckBox checkFTpsMonitoring,fSize=12,value= 0
	SetVariable setvarSkidTpsMonitoring,pos={205,55},size={100,19},title=" "
	SetVariable setvarSkidTpsMonitoring,fSize=12,limits={-inf,inf,0}, value = root:SKID:Variables:tpsTotMonit
	
	Button buttonSkidFermer,pos={515,72},size={100,20},proc=ButtonProc_skidFermer,title="Fermer"

	return 0
end


// Cr�ation chemin/r�pertoire acceuillant les donn�es 
Function SetVarProc_skidPath(ctrlName,varNum,varStr,varName) : SetVariableControl
	String ctrlName
	Variable varNum
	String varStr
	String varName
	
	SVAR skidPath = root:SKID:Variables:skidPath
	
	// V�rif si ":" oubli�
	if(cmpstr(varStr[strlen(varStr) - 1], ":") != 0)
	
		varStr = varStr + ":"
	endif
	
	skidPath = varStr		// MAJ skidPath pour enregistrement skidPath sur disque (skidPathNfile.txt)
		
	creatCheminRep(varStr)
	
	return 0
End



Function ButtonProc_skidGo(ctrlName) : ButtonControl
	String ctrlName
	
	// V�rif : oubli de saisie de la valeur du temps de monitorage
	controlInfo /W=skidPanel checkFTpsMonitoring
	if(V_Value == 1)
	
		NVAR tpsTotMonit = root:SKID:Variables:tpsTotMonit
		if(tpsTotMonit <= 0)
		
			doAlert /T="Oubli !" 0, "Temps monitoring = 0 !!!"
			
			return 0  
		endif
	endif
	
	// MAJ �tat IHM
	NVAR t0 = root:SKID:Variables:t0 ; t0 = dateTime	// clic d�part (temps de r�f�rence)
	SetVariable setvarSkidPath win = skidPanel, disable = 2
	Button buttonSkidGo win = skidPanel, disable = 2
	Button buttonSkidGo win = skidPanel, title="\\W648"
	Button buttonSkidStop win = skidPanel, disable = 0
	Button buttonSkidStop win = skidPanel, title="\\W616"
	CheckBox checkFTpsMonitoring win = skidPanel, disable = 2
	SetVariable setvarSkidTpsMonitoring win = skidPanel, disable = 2
	Button buttonSkidFermer win = skidPanel, disable = 2
	
	SVAR mode = root:SKID:Variables:mode
	
	strSwitch(mode)
	
		case "monitoring" :
			
			skid_reinitWaves_()
			skid_setFormulaON()				// Liaisons d�pendances
			skid_startMonitoring()			// D�marrage monitorage		
			break
			
		case "background" :
		
			// plus la peine...
			break
			
		case "fake" :
			
			skid_reinitWaves_()
			skid_fakeBckGround()
			break

	endSwitch
	

		
	return 0
End



Function ButtonProc_skidStop(ctrlName) : ButtonControl
	String ctrlName
	
	// MAJ �tat IHM
	SetVariable setvarSkidPath win = skidPanel, disable = 0
	Button buttonSkidGo win = skidPanel, disable = 0
	Button buttonSkidGo win = skidPanel, title="\\W649"
	Button buttonSkidStop win = skidPanel, disable = 2
	Button buttonSkidStop win = skidPanel, title="\\W605"
	CheckBox checkFTpsMonitoring win = skidPanel, disable = 0
	SetVariable setvarSkidTpsMonitoring win = skidPanel, disable = 0
	Button buttonSkidFermer win = skidPanel, disable = 0
	
	SVAR mode = root:SKID:Variables:mode
	
	strSwitch(mode)
	
		case "monitoring" :

			skid_stopMonitoring()			// Arr�t monitorage
			skid_setFormulaOFF()			// Rupture liaisons d�pendances
			//skid_reinitWaves_()				// Remise dim des Wave_ � 0		
			break
			
		case "background" :
		
			break
			
		case "fake" :
			
				ctrlBackground stop
				killBackground
				//sauvegardeDonnees()
				NVAR compteur = root:SKID:Variables:compteur
				compteur = 0					// R�initialisation compteur
			break

	endSwitch
	
	sauvegardeDonnees()				// sauvegarde des waves au format : nomWave - Date - Heure.txt
	skid_reinitWaves_()				// Remise dim des Wave_ � 0
	
	return 0
End



Function ButtonProc_skidFermer(ctrlName) : ButtonControl
	String ctrlName
	
	doAlert /T= "Fermeture" 1, "Quitter skid ?"
	if (V_flag == 2)
	
		print "V_flag -->", V_flag
		return 0
	endif
	
	savePathNfile()

	doWindow /K pressionsPlot
	doWindow /K temperaturePlot
	doWindow /K vannePlot
	killWindow skidPanel
	
	killDataFolder /Z root:SKID
	
	return 0
End




PRESSIONS _____________________________________________________________________________________________________

function skid_IHMpressions()

	doWindow /F pressionsPlot
	if(V_flag == 1)
	
		return 0
	endif

	WAVE Wave_mp201d = root:SKID:Variables:Wave_mp201d
	WAVE Wave_mp202d = root:SKID:Variables:Wave_mp202d
	WAVE Wave_mp205d = root:SKID:Variables:Wave_mp205d
	WAVE Wave_mp203r = root:SKID:Variables:Wave_mp203r
	WAVE Wave_mp204r = root:SKID:Variables:Wave_mp204r
	WAVE Wave_mp206r = root:SKID:Variables:Wave_mp206r
	WAVE Wave_mp207r = root:SKID:Variables:Wave_mp207r
	
	
	
	Display /N=pressionsPlot /W=(1,46,853,489) as "Skid - Pressions"
	
	AppendToGraph /W = pressionsPlot Wave_mp201d[][1] vs Wave_mp201d[][0]
	AppendToGraph /W = pressionsPlot Wave_mp202d[][1] vs Wave_mp202d[][0]
	AppendToGraph /W = pressionsPlot Wave_mp205d[][1] vs Wave_mp205d[][0]
	
	AppendToGraph /W = pressionsPlot Wave_mp203r[][1] vs Wave_mp203r[][0]
	AppendToGraph /W = pressionsPlot Wave_mp204r[][1] vs Wave_mp204r[][0]
	AppendToGraph /W = pressionsPlot Wave_mp206r[][1] vs Wave_mp206r[][0]
	AppendToGraph /W = pressionsPlot Wave_mp207r[][1] vs Wave_mp207r[][0]
	
	// Epaisseur des trac�s
	ModifyGraph /W=pressionsPlot /Z lSize(Wave_mp201d)=1.5,lSize(Wave_mp202d)=1.5,lSize(Wave_mp205d)=1.5
	ModifyGraph /W=pressionsPlot /Z lSize(Wave_mp203r)=1.5,lSize(Wave_mp204r)=1.5,lSize(Wave_mp206r)=1.5,lSize(Wave_mp207r)=1.5
	
	// Style des trac�s
	// trait plein pour les Wave_mpXYZd
	ModifyGraph /W=pressionsPlot /Z lStyle(Wave_mp201d)=0,lStyle(Wave_mp202d)=0,lStyle(Wave_mp205d)=0
	// Pointill�s pour les Wave_mpXYZr
	ModifyGraph /W=pressionsPlot /Z lStyle(Wave_mp203r)=2,lStyle(Wave_mp204r)=2,lStyle(Wave_mp206r)=2,lStyle(Wave_mp207r)=2
	
	// Couleurs des trac�s
	// Wave_mp201d et Wave_mp203r sont en rouge, la couleur par d�faut --> pas oblig� de les mentioner
	// ModifyGraph  /W=pressionsPlot /Z rgb(Wave_mp201d)=(0,0,65535)					// Rouge
	ModifyGraph /W=pressionsPlot /Z rgb(Wave_mp202d)=(1,52428,26586)				// vert
	ModifyGraph /W=pressionsPlot /Z rgb(Wave_mp205d)=(0,0,65535)					// bleu
	// ModifyGraph  /W=pressionsPlot /Z rgb(Wave_mp203r)=(0,0,65535)					// Rouge
	ModifyGraph /W=pressionsPlot /Z rgb(Wave_mp204r)=(1,52428,26586)				// vert
	ModifyGraph /W=pressionsPlot /Z rgb(Wave_mp206r)=(0,0,65535)					// bleu
	ModifyGraph /W=pressionsPlot /Z rgb(Wave_mp207r)=(17476,17476,17476)		// gris fonc�
	
	// Titres abcisses / Ordonn�es
	Label /W=pressionsPlot left "\\Z18Pression"
	Label /W=pressionsPlot bottom "\\Z18x mn / point"
	
	
	
	// Correspondance trac�s <=> waves
	Legend
	//Legend/C/N=text0/J "\\s(wave1) wave1\r\\s(wave2) wave2"
	
	// Bandeau checkBox
	ControlBar /W=pressionsPlot 70
	CheckBox check_mp201d,pos={12,9},size={67,16},proc=CheckProc_pressions,title="mp201d"
	CheckBox check_mp201d,fSize=12,value= 1
	CheckBox check_mp202d,pos={12,26},size={67,16},proc=CheckProc_pressions,title="mp202d"
	CheckBox check_mp202d,fSize=12,value= 1
	CheckBox check_mp205d,pos={12,44},size={67,16},proc=CheckProc_pressions,title="mp205d"
	CheckBox check_mp205d,fSize=12,value= 1
	CheckBox check_mp203r,pos={94,9},size={65,16},proc=CheckProc_pressions,title="mp203r"
	CheckBox check_mp203r,fSize=12,value= 1
	CheckBox check_mp204r,pos={94,26},size={65,16},proc=CheckProc_pressions,title="mp204r"
	CheckBox check_mp204r,fSize=12,value= 1
	CheckBox check_mp206r,pos={94,44},size={65,16},proc=CheckProc_pressions,title="mp206r"
	CheckBox check_mp206r,fSize=12,value= 1
	CheckBox check_mp207r,pos={170,9},size={65,16},proc=CheckProc_pressions,title="mp207r"
	CheckBox check_mp207r,fSize=12,value= 1

	SetDrawLayer UserFront

	return 0
end


// Show / Hide pressions
Function CheckProc_pressions(ctrlName,checked) : CheckBoxControl
	String ctrlName
	Variable checked
	
//	WAVE Wave_mp201d = root:SKID:Variables:Wave_mp201d
//	WAVE Wave_mp202d = root:SKID:Variables:Wave_mp202d
//	WAVE Wave_mp205d = root:SKID:Variables:Wave_mp205d
//	WAVE Wave_mp203r = root:SKID:Variables:Wave_mp203r
//	WAVE Wave_mp204r = root:SKID:Variables:Wave_mp204r
//	WAVE Wave_mp206r = root:SKID:Variables:Wave_mp206r
//	WAVE Wave_mp207r = root:SKID:Variables:Wave_mp207r
	
	
	strSwitch(ctrlName)
	
		case "check_mp201d" :
			controlinfo /W=pressionsPlot check_mp201d
				if (V_Value == 0)
					
					ModifyGraph /W=pressionsPlot hideTrace(Wave_mp201d) = 2
				else
					
					ModifyGraph /W=pressionsPlot hideTrace(Wave_mp201d) = 0
				endif
			break
			
		case "check_mp202d" :
			controlinfo /W=pressionsPlot check_mp202d
				if (V_Value == 0)
				
					ModifyGraph /W=pressionsPlot hideTrace(Wave_mp202d) = 2
				else
				
					ModifyGraph /W=pressionsPlot hideTrace(Wave_mp202d) = 0
				endif		
			break
			
		case "check_mp205d" :
			controlinfo /W=pressionsPlot check_mp205d
				if (V_Value == 0)
				
					ModifyGraph /W=pressionsPlot hideTrace(Wave_mp205d) = 2
				else
				
					ModifyGraph /W=pressionsPlot hideTrace(Wave_mp205d) = 0
				endif		
			break
			
		case "check_mp203r" :
			controlinfo /W=pressionsPlot check_mp203r
				if (V_Value == 0)
				
					ModifyGraph /W=pressionsPlot hideTrace(Wave_mp203r) = 2
				else
					
					ModifyGraph /W=pressionsPlot hideTrace(Wave_mp203r) = 0
				endif		
			break
			
		case "check_mp204r" :
			controlinfo /W=pressionsPlot check_mp204r
				if (V_Value == 0)
				
					ModifyGraph /W=pressionsPlot hideTrace(Wave_mp204r) = 2
				else
				
					ModifyGraph /W=pressionsPlot hideTrace(Wave_mp204r) = 0
				endif		
			break
			
		case "check_mp206r" :
			controlinfo /W=pressionsPlot check_mp206r
				if (V_Value == 0)
				
					ModifyGraph /W=pressionsPlot hideTrace(Wave_mp206r) = 2
				else
					
					ModifyGraph /W=pressionsPlot hideTrace(Wave_mp206r) = 0
				endif		
			break
			
		case "check_mp207r" :
			controlinfo /W=pressionsPlot check_mp207r
				if (V_Value == 0)
				
					ModifyGraph /W=pressionsPlot hideTrace(Wave_mp207r) = 2
				else
				
					ModifyGraph /W=pressionsPlot hideTrace(Wave_mp207r) = 2
				endif		
			break
	
	endswitch
	
	return 0
End




TEMPERATURES _____________________________________________________________________________________________________

function skid_IHMtemperatures()

	doWindow /F temperaturePlot
	if(V_flag == 1)
	
		return 0
	endif

	WAVE Wave_mt201d = root:SKID:Variables:Wave_mt201d
	WAVE Wave_mt203d = root:SKID:Variables:Wave_mt203d
	WAVE Wave_mt205d = root:SKID:Variables:Wave_mt205d
	
	WAVE Wave_mt202r = root:SKID:Variables:Wave_mt202r
	WAVE Wave_mt204r = root:SKID:Variables:Wave_mt204r
	WAVE Wave_mt206r = root:SKID:Variables:Wave_mt206r
	
	
	
	
	Display /N=temperaturePlot /W=(855,46,1707,489) as "Skid - Temp�ratures"
	
	AppendToGraph /W = temperaturePlot Wave_mt201d[][1] vs Wave_mt201d[][0]
	AppendToGraph /W = temperaturePlot Wave_mt203d[][1] vs Wave_mt203d[][0]
	AppendToGraph /W = temperaturePlot Wave_mt205d[][1] vs Wave_mt205d[][0]
	
	AppendToGraph /W = temperaturePlot Wave_mt202r[][1] vs Wave_mt202r[][0]
	AppendToGraph /W = temperaturePlot Wave_mt204r[][1] vs Wave_mt204r[][0]
	AppendToGraph /W = temperaturePlot Wave_mt206r[][1] vs Wave_mt206r[][0]
	
	// Epaisseur des trac�s
	ModifyGraph /W=temperaturePlot /Z lSize(Wave_mt201d)=1.5,lSize(Wave_mt203d)=1.5,lSize(Wave_mt205d)=1.5
	ModifyGraph /W=temperaturePlot /Z lSize(Wave_mt202r)=1.5,lSize(Wave_mt204r)=1.5,lSize(Wave_mt206r)=1.5
	
	// Style des trac�s
	// trait plein pour les Wave_mpXYZd
	ModifyGraph /W=temperaturePlot /Z lStyle(Wave_mt201d)=0,lStyle(Wave_mt203d)=0,lStyle(Wave_mt205d)=0
	// Pointill�s pour les Wave_mpXYZr
	ModifyGraph /W=temperaturePlot /Z lStyle(Wave_mt202r)=2,lStyle(Wave_mt204r)=2,lStyle(Wave_mt206r)=2
	
	// Couleurs des trac�s
	// Wave_mt201d et Wave_mt202r sont en rouge, la couleur par d�faut
	ModifyGraph /W=temperaturePlot /Z rgb(Wave_mt203d)=(1,52428,26586)			// vert
	ModifyGraph /W=temperaturePlot /Z rgb(Wave_mt205d)=(0,0,65535)					// bleu
	ModifyGraph /W=temperaturePlot /Z rgb(Wave_mt204r)=(1,52428,26586)			// vert
	ModifyGraph /W=temperaturePlot /Z rgb(Wave_mt206r)=(0,0,65535)					// bleu
		
	// Titres abcisses / Ordonn�es
	Label /W=temperaturePlot left "\\Z18Temperature"
	Label /W=temperaturePlot bottom "\\Z18x mn / point"
	
	
	
	// Correspondance trac�s <=> waves
	Legend
	//Legend/C/N=text0/J "\\s(wave1) wave1\r\\s(wave2) wave2"
	
	// Bandeau checkBox
	ControlBar /W=temperaturePlot 70
	CheckBox check_mt201d,pos={12,9},size={67,16},proc=CheckProc_temperatures,title="mt201d"
	CheckBox check_mt201d,fSize=12,value= 1
	CheckBox check_mt203d,pos={12,26},size={67,16},proc=CheckProc_temperatures,title="mt203d"
	CheckBox check_mt203d,fSize=12,value= 1
	CheckBox check_mt205d,pos={12,44},size={67,16},proc=CheckProc_temperatures,title="mt205d"
	CheckBox check_mt205d,fSize=12,value= 1

	CheckBox check_mt202r,pos={94,9},size={65,16},proc=CheckProc_temperatures,title="mt202r"
	CheckBox check_mt202r,fSize=12,value= 1
	CheckBox check_mt204r,pos={94,26},size={65,16},proc=CheckProc_temperatures,title="mt204r"
	CheckBox check_mt204r,fSize=12,value= 1
	CheckBox check_mt206r,pos={94,44},size={65,16},proc=CheckProc_temperatures,title="mt206r"
	CheckBox check_mt206r,fSize=12,value= 1
	
	SetDrawLayer UserFront

	return 0
end


// Show / Hide t�emp�ratures
Function CheckProc_temperatures(ctrlName,checked) : CheckBoxControl
	String ctrlName
	Variable checked
	
	
	strSwitch(ctrlName)
	
		case "check_mt201d" :
			controlinfo /W=temperaturePlot check_mt201d
				if (V_Value == 0)
				
					ModifyGraph /W=temperaturePlot hideTrace(Wave_mt201d) = 2
				else
					
					ModifyGraph /W=temperaturePlot hideTrace(Wave_mt201d) = 0
				endif
			break
			
		case "check_mt203d" :
			controlinfo /W=temperaturePlot check_mt203d
				if (V_Value == 0)
				
					ModifyGraph /W=temperaturePlot hideTrace(Wave_mt203d) = 2
				else
					
					ModifyGraph /W=temperaturePlot hideTrace(Wave_mt203d) = 0
				endif		
			break
			
		case "check_mt205d" :
			controlinfo /W=temperaturePlot check_mt205d
				if (V_Value == 0)
				
					ModifyGraph /W=temperaturePlot hideTrace(Wave_mt205d) = 2
				else
				
					ModifyGraph /W=temperaturePlot hideTrace(Wave_mt205d) = 0
				endif		
			break
			
		case "check_mt202r" :
			controlinfo /W=temperaturePlot check_mt202r
				if (V_Value == 0)
				
					ModifyGraph /W=temperaturePlot hideTrace(Wave_mt202r) = 2
				else
					
					ModifyGraph /W=temperaturePlot hideTrace(Wave_mt202r) = 0
				endif		
			break
			
		case "check_mt204r" :
			controlinfo /W=temperaturePlot check_mt204r
				if (V_Value == 0)
				
					ModifyGraph /W=temperaturePlot hideTrace(Wave_mt204r) = 2
				else
				
					ModifyGraph /W=temperaturePlot hideTrace(Wave_mt204r) = 0
				endif		
			break
			
		case "check_mt206r" :
			controlinfo /W=temperaturePlot check_mt206r
				if (V_Value == 0)
				
					ModifyGraph /W=temperaturePlot hideTrace(Wave_mt206r) = 2
				else
				
					ModifyGraph /W=temperaturePlot hideTrace(Wave_mt206r) = 0
				endif		
			break
	
	endswitch
	
	return 0
End





FLUX __________________________________________________________________________________________________________

function skid_IHMflux()


	doWindow /F vannePlot
	if(V_flag == 1)
	
		return 0
	endif

	WAVE Wave_mt204vanne = root:SKID:Variables:Wave_mt204vanne

	Display /N=vannePlot /W=(1,514,853,957) as "Skid - Vanne/Flux"
	ControlBar /W=vannePlot 70
	
	AppendToGraph /W = vannePlot Wave_mt204vanne[][1] vs Wave_mt204vanne[][0]

	
	// Epaisseur des trac�s
	ModifyGraph /W=vannePlot /Z lSize(Wave_mt204vanne)=1.5
		
	// Style des trac�s
	// trait plein pour les Wave_mpXYZd
	ModifyGraph /W=vannePlot /Z lStyle(Wave_mt204vanne)=0

		
	// Titres abcisses / Ordonn�es
	Label /W=vannePlot left "\\Z18Flux"
	Label /W=vannePlot bottom "\\Z18x mn / point"
	
	Legend
	
	return 0
end








//___________________________________________________________________________________________________


function testwaves()

	Make /O /N=21 noise1=gnoise(1) + 10, noise2=gnoise(2) + 10, noise3=gnoise(3) + 10
	Make /O /N=21 noise_1=gnoise(1), noise_2=gnoise(1), noise_3=gnoise(1), noise_4=gnoise(1)

	Display /W=(498,167,1435,732) noise1, noise2, noise3, noise_1, noise_2, noise_3, noise_4
	
	
	ModifyGraph lSize(noise_1)=1.5,lSize(noise_2)=1.5,lSize(noise_3)=1.5,lSize(noise_4)=1.5
	
	ModifyGraph lStyle(noise_1)=2,lStyle(noise_2)=2,lStyle(noise_3)=2,lStyle(noise_4)=2
	
	
	ModifyGraph rgb(noise2)=(1,52428,26586),rgb(noise3)=(0,0,65535),rgb(noise_2)=(1,52428,26586)
	
	ModifyGraph rgb(noise_3)=(0,0,65535),rgb(noise_4)=(17476,17476,17476)
	
	
	
	Label left "\\Z18Pression"
	Label bottom "\\Z18x mn / point"
	
	Legend/C/N=text0/J/X=-3.63/Y=-2.23 "\\s(noise1) noise1\r\\s(noise2) noise2\r\\s(noise3) noise3\r\\s(noise_1) noise_1\r\\s(noise_2) noise_2\r\\s(noise_3) noise_3\r\\s(noise_4) noise_4"
	//AppendText "\\s(noise_4) noise_4"

	return 0
end