#pragma rtGlobals=3		// Use modern global access method and strict wave access.

_____________

MONITORING
SET FORMULA
SAVE PATH
_____________



________________________________________ MONITORING ________________________________________


// START MONITORING --> P, T�, Vanne
function skid_startMonitoring()		
	
	variable refresh		// refresh = 10 chez Cassiop�e
	
	SVAR adrPressureList = root:SKID:Variables:adrPressureList
	SVAR varPressure  = root:SKID:Variables:varPressure
	
	SVAR adrTemperatureList = root:SKID:Variables:adrTemperatureList
	SVAR varTemperature = root:SKID:Variables:varTemperature
	
	string varPath = "root:SKID:Variables:"
	
	variable i, nbElements
	
	
// Pressure
	nbElements = itemsInList(adrPressureList, ";")
	for (i = 0 ; i <= nbElements - 1 ; i += 1)
	
		if(tango_start_attr_monitor(stringFromList(i,adrPressureList, ";"), "pressure", varPath + stringFromList(i,varPressure, ";"), refresh) < 0)
			DoAlert 0, "Monitoring error on :" + stringFromList(i,adrPressureList, ";")
			return 0
		endif
	endfor
	
	
// Temperature
	nbElements = itemsInList(adrTemperatureList, ";")
	for (i = 0 ; i <= nbElements - 1 ; i += 1)
	
		if(tango_start_attr_monitor(stringFromList(i,adrTemperatureList, ";"), "temperature", varPath + stringFromList(i,varTemperature, ";"), refresh) < 0)
			DoAlert 0, "Monitoring error on :" + stringFromList(i,adrTemperatureList, ";")
			return 0
		endif
	endfor
	
	
// Vanne
		if(tango_start_attr_monitor("sli-skid/gtc/mt204-vanne", "ouverture", varPath + "mt204vanne", refresh) < 0)
			DoAlert 0, "Monitoring error on :" + "sli-skid/gtc/mt204-vanne"
			return 0
		endif

	return 0
end

//// Factice : pour faire marcher skid_startMonitoring() sans binding
//function tango_start_attr_monitor(devAdress, devAttribute, IgorVarPath, refresh)
//	string devAdress
//	string devAttribute
//	string IgorVarPath
//	variable refresh
//	
//	
//	return 0
//end
//	
	
//_____________________________________________________________________________________
	
	
// STOP MONITORING --> P, T�, Vanne
function skid_stopMonitoring()
	
	variable TOTO = -1
	
	SVAR adrPressureList = root:SKID:Variables:adrPressureList
	SVAR adrTemperatureList = root:SKID:Variables:adrTemperatureList
	
	string varPath = "root:SKID:Variables:"
	
	variable i, nbElements
	
	
// Pressure
	nbElements = itemsInList(adrPressureList, ";")
	for (i = 0 ; i <= nbElements - 1 ; i += 1)
	
		if (tango_stop_attr_monitor(stringFromList(i,adrPressureList, ";"), "pressure", TOTO)<0)
			DoAlert 0, "Erreur d'arr�t du monitorage sur le device : " + stringFromList(i,adrPressureList, ";")
			return 0
		endif
	endfor
		
		
// Temperature
	nbElements = itemsInList(adrTemperatureList, ";")
	for (i = 0 ; i <= nbElements - 1 ; i += 1)
	
		if (tango_stop_attr_monitor(stringFromList(i,adrTemperatureList, ";"), "temperature", TOTO)<0)
			DoAlert 0, "Erreur d'arr�t du monitorage sur le device : " + stringFromList(i,adrTemperatureList, ";")
			return 0
		endif
	endfor
	
	
// Vanne
		if (tango_stop_attr_monitor("sli-skid/gtc/mt204-vanne", "ouverture", TOTO)<0)
			DoAlert 0, "Erreur d'arr�t du monitorage sur le device : " + "sli-skid/gtc/mt204-vanne"
			return 0
		endif	
		
	
	return 0
end

//// Factice : pour faire marcher skid_stopMonitoring() sans binding
//function tango_stop_attr_monitor(devAdress, devAttribute, TOTO)
//	string devAdress
//	string devAttribute
//	variable TOTO
//	
//	return 0
//end

---------------------------------------- MONITORING ----------------------------------------







________________________________________ SET FORMULA ________________________________________


// On rend d�pendant
function skid_setFormulaON()

	string previous_DF = getDataFolder(1)
	
		setDataFolder root:SKID:Variables
		
			// Pression
			NVAR mp201d, mp202d, mp205d, mp203r, mp204r, mp206r, mp207r
			NVAR SF_mp201d, SF_mp202d, SF_mp205d, SF_mp203r, SF_mp204r, SF_mp206r, SF_mp207r
			setFormula SF_mp201d, "skid_setFor_mp201d(mp201d)"
			setFormula SF_mp202d, "skid_setFor_mp202d(mp202d)"
			setFormula SF_mp205d, "skid_setFor_mp205d(mp205d)"
			setFormula SF_mp203r, "skid_setFor_mp203r(mp203r)"
			setFormula SF_mp204r, "skid_setFor_mp204r(mp204r)"
			setFormula SF_mp206r, "skid_setFor_mp206r(mp206r)"
			setFormula SF_mp207r, "skid_setFor_mp207r(mp207r)"
			
			// T�emp�rature
			NVAR mt201d, mt203d, mt205d, mt202r, mt204r, mt206r
			NVAR SF_mt201d, SF_mt203d, SF_mt205d, SF_mt202r, SF_mt204r, SF_mt206r
			setFormula SF_mt201d, "skid_setFor_mt201d(mt201d)"
			setFormula SF_mt203d, "skid_setFor_mt203d(mt203d)"
			setFormula SF_mt205d, "skid_setFor_mt205d(mt205d)"
			setFormula SF_mt202r, "skid_setFor_mt202r(mt202r)"
			setFormula SF_mt204r, "skid_setFor_mt204r(mt204r)"
			setFormula SF_mt206r, "skid_setFor_mt206r(mt206r)"
			
			// Flux T� (vanne)
			NVAR mt204vanne
			NVAR SF_mt204vanne
			setFormula SF_mt204vanne, "skid_setFor_mt204vanne(mt204vanne)"

		setDataFolder previous_df
	
	return 0
end


// On l�ve la d�pendance
function skid_setFormulaOFF()

	string previous_DF = getDataFolder(1)
	
		setDataFolder root:SKID:Variables
		
		// Pression
			NVAR SF_mp201d, SF_mp202d, SF_mp205d, SF_mp203r, SF_mp204r, SF_mp206r, SF_mp207r
			setFormula SF_mp201d, ""
			setFormula SF_mp202d, ""
			setFormula SF_mp205d, ""
			setFormula SF_mp203r, ""
			setFormula SF_mp204r, ""
			setFormula SF_mp206r, ""
			setFormula SF_mp207r, ""		
		
		// T�emp�rature
			NVAR SF_mt201d, SF_mt203d, SF_mt205d, SF_mt202r, SF_mt204r, SF_mt206r
			setFormula SF_mt201d, ""
			setFormula SF_mt203d, ""
			setFormula SF_mt205d, ""
			setFormula SF_mt202r, ""
			setFormula SF_mt204r, ""
			setFormula SF_mt206r, ""			
		
			// Flux T� (vanne)
			NVAR SF_mt204vanne
			setFormula SF_mt204vanne, ""
			
		setDataFolder previous_df
		
	return 0
end

---------------------------------------- SET FORMULA ----------------------------------------








________________________________________ PAR BACKGROUND ________________________________________

---------------------------------------- PAR BACKGROUND ----------------------------------------







____________________________________________ FAKE ______________________________________________

function skid_fakeBckGround()

	killBackground		// Par s�curit�

	NVAR t0 = root:SKID:Variables:t0 ; t0 = dateTime		// chrono d�part
	Setbackground skid_fakeWaves()
	ctrlBackground start, period = 60

	return 0
end


function skid_fakeWaves()

	string previous_DF = getDataFolder(1)
	
		setDataFolder root:SKID:Variables
		
			NVAR t0
			NVAR compteur ; compteur += 1
		
		
			// Pressions
			WAVE Wave_mp201d, Wave_mp202d, Wave_mp205d, Wave_mp203r, Wave_mp204r, Wave_mp206r, Wave_mp207r
			
			redimension /N=(compteur, 2) Wave_mp201d
			Wave_mp201d[compteur - 1][0] = dateTime - t0
			Wave_mp201d[compteur - 1][1] = 20 + compteur + gnoise(1)
			
			redimension /N=(compteur, 2) Wave_mp202d
			Wave_mp202d[compteur - 1][0] = dateTime - t0
			Wave_mp202d[compteur - 1][1] = 20 + compteur + gnoise(1)
			
			redimension /N=(compteur, 2) Wave_mp205d
			Wave_mp205d[compteur - 1][0] = dateTime - t0
			Wave_mp205d[compteur - 1][1] = 20 + compteur + gnoise(1)
			
			redimension /N=(compteur, 2) Wave_mp203r
			Wave_mp203r[compteur - 1][0] = dateTime - t0
			Wave_mp203r[compteur - 1][1] = compteur + gnoise(1)
		
			redimension /N=(compteur, 2) Wave_mp204r
			Wave_mp204r[compteur - 1][0] = dateTime - t0
			Wave_mp204r[compteur - 1][1] = compteur + gnoise(1)
		
			redimension /N=(compteur, 2) Wave_mp206r
			Wave_mp206r[compteur - 1][0] = dateTime - t0
			Wave_mp206r[compteur - 1][1] = compteur + gnoise(1)
		
			redimension /N=(compteur, 2) Wave_mp207r
			Wave_mp207r[compteur - 1][0] = dateTime - t0
			Wave_mp207r[compteur - 1][1] = compteur + gnoise(1)
			
			
				
			// T�emp�ratures
			WAVE Wave_mt201d, Wave_mt203d, Wave_mt205d, Wave_mt202r, Wave_mt204r, Wave_mt206r
			
			redimension /N=(compteur, 2) Wave_mt201d
			Wave_mt201d[compteur - 1][0] = dateTime - t0
			Wave_mt201d[compteur - 1][1] = 30 + compteur + gnoise(2)
			
			redimension /N=(compteur, 2) Wave_mt203d
			Wave_mt203d[compteur - 1][0] = dateTime - t0
			Wave_mt203d[compteur - 1][1] = 30 + compteur + gnoise(2)
			
			redimension /N=(compteur, 2) Wave_mt205d
			Wave_mt205d[compteur - 1][0] = dateTime - t0
			Wave_mt205d[compteur - 1][1] = 30 + compteur + gnoise(2)
			
			redimension /N=(compteur, 2) Wave_mt202r
			Wave_mt202r[compteur - 1][0] = dateTime - t0
			Wave_mt202r[compteur - 1][1] = compteur + gnoise(2)
		
			redimension /N=(compteur, 2) Wave_mt204r
			Wave_mt204r[compteur - 1][0] = dateTime - t0
			Wave_mt204r[compteur - 1][1] = compteur + gnoise(2)
		
			redimension /N=(compteur, 2) Wave_mt206r
			Wave_mt206r[compteur - 1][0] = dateTime - t0
			Wave_mt206r[compteur - 1][1] = compteur + gnoise(2)
			
			
			// Flux
			WAVE Wave_mt204vanne
			redimension /N=(compteur, 2) Wave_mt204vanne
			Wave_mt204vanne[compteur - 1][0] = dateTime - t0
			Wave_mt204vanne[compteur - 1][1] = compteur + gnoise(1)
				
		setDataFolder previous_df

	return 0
end

-------------------------------------------- FAKE ----------------------------------------------








____________________________________ REINITIALISATION Wave_ ____________________________________

function skid_reinitWaves_()

	string previous_DF = getDataFolder(1)
	
		setDataFolder root:SKID:Variables

		string wavesList = waveList("Wave_*", ";", "")
		variable wavesListLen = itemsInList(wavesList, ";")
		variable i
		
		for(i = 0 ; i <= wavesListLen - 1 ; i += 1)
		
			// Remise des Wave_ � dim = 0
			redimension /N=(0, 2) $stringFromList(i, wavesList, ";")
			
		endfor

		setDataFolder previous_df

	return 0
end
------------------------------------ REINITIALISATION Wave_ -----------------------------------








________________________________________ SAUVEGARDE PATH ______________________________________

// Sauvegarde du path
function savePathNfile()

	SVAR skidPath = root:SKID:Variables:skidPath
	WAVE /T skidPathNfile = root:SKID:Variables:skidPathNfile
	
	skidPathNfile [0] = skidPath
	skidPathNfile [1] = "prefixFile"		// L� mais pas utilis�
	
	Save /W/O/J/M="\r\n" skidPathNfile as "D:PRGMs_Igor:Skid:skidPathNfile.txt"

	return 0
end

---------------------------------------- SAUVEGARDE PATH --------------------------------------






___________________________________ CREATION CHEMIN REPERTOIRE __________________________________

function creatCheminRep(chemin)
	string chemin
	
	// Dabord v�rifier si - chemin - n'existe pas d�j�
	NewPath /O /Z /C /Q nomSymbolique chemin
	if(V_flag == 0)
	
		print "Existe"
		return 0	// Si existe
	else
		
		// Si n'existe pas
		variable NbRepertoires = itemsInList(chemin, ":")
		variable i
		string pathEvol = "hd:"
		
		for(i = 1 ; i <= NbRepertoires - 1 ; i += 1)
		
			pathEvol += stringFromList(i, chemin, ":") + ":" ; print "pathEvol :", pathEvol
			
			NewPath /O /Z /C /Q nomChemin pathEvol 
		endfor		
	endif
	
	return 0
end
----------------------------------- CREATION CHEMIN REPERTOIRE ----------------------------------






______________________________________ SAUVEGARDE DONNEES ______________________________________


function sauvegardeDonnees()


	string previous_df = getdatafolder(1)
	
		setDataFolder root:SKID:Variables
		
			// Chemin de sauvegarde
			WAVE /T skidPathNfile
			
			string wavesList = wavelist("wave_*", ";", "")
			variable i
			
			for (i = 0 ; i <= itemsInList(wavesList, ";") - 1 ; i+=1)
		
				save /O /J $stringFromList(i, wavesList, ";") as skidPathNfile[0] + stringFromList(i, wavesList, ";") + " - " + date() + " - " + skid_time() + ".txt"
			endfor

		setDataFolder previous_df

	return 0
end


function /T skid_time()		// Passage de ":" � "." dans le retour de la fontion time(). Avec ":", l'OS n'aime pas.

	string heure = time() 
	variable i
	
	for (i = 0 ; i <= strlen(heure) - 1 ; i += 1)
	
		if(cmpstr(heure[i, i], ":") == 0)
		
			heure[i, i] = "."
		endif
	endfor
	
	return heure
end

-------------------------------------- SAUVEGARDE DONNEES --------------------------------------





______________________________________ APPEL GRAPHES ______________________________________

function skid_showGraph(nomGraphe)
	string nomGraphe
	
	if(dataFolderExists("root:SKID") == 0)
	
		doAlert /T="" 0, "D�marrez Skid : menu Igor > Skid > Skid"
		return 0
	endif
	
	strSwitch(nomGraphe)
	
		case "Pression" :
			skid_IHMpressions()
			break
			
		case "Temperature" :
			skid_IHMtemperatures()
			break
			
		case "Flux" :
			skid_IHMflux()
			break
			
	endSwitch
	
	return 0
end

-------------------------------------- APPEL GRAPHES --------------------------------------








______________________________________ ARRET D'URGENCE ______________________________________
function skid_arretUrgent()

	// MAJ �tat IHM
	Button buttonSkidGo win = skidPanel, disable = 0
	Button buttonSkidStop win = skidPanel, disable = 2
	CheckBox checkFTpsMonitoring win = skidPanel, disable = 0
	SetVariable setvarSkidTpsMonitoring win = skidPanel, disable = 0
	Button buttonSkidFermer win = skidPanel, disable = 0
	
//	skid_stopMonitoring()			// Arr�t monitorage
//	skid_setFormulaOFF()			// Rupture liaisons d�pendances

	return 0
end

-------------------------------------- ARRET D'URGENCE --------------------------------------





//__________________________________________________________________________________________________
// Juste pour cr�ation pour dev appli
function create_savePathNfile()


	Make /T /N=2 skidPathNfile ; WAVE /T skidPathNfile = root:skidPathNfile
	
	skidPathNfile [0] = "c:skid:"
	skidPathNfile [1] = "prefixFile"
	
	Save /W/O/J/M="\r\n" skidPathNfile as "D:PRGMs_Igor:Skid:skidPathNfile.txt"

	return 0
end




	
