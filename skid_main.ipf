#pragma rtGlobals=3		// Use modern global access method and strict wave access.


menu "Skid"

	"Skid", skid_Init()
	"-"
	"IHM pressions", skid_showGraph("Pression")
	"IHM temp�ratures", skid_showGraph("Temperature")
	"IHM flux", skid_showGraph("Flux")
	"-"
	"Arr�t urgent !", skid_arretUrgent()
end
 


//// D�pendances fichiers (Evry)
//#include "hd:Users:racine:Desktop:Confi nov 20:prgm_Igor:Skid:skid_IHM"
//#include "hd:Users:racine:Desktop:Confi nov 20:prgm_Igor:Skid:skid_Variables"
//#include "hd:Users:racine:Desktop:Confi nov 20:prgm_Igor:Skid:skid_Utilities"
//
//#include "hd:Users:racine:Desktop:Confi nov 20:prgm_Igor:Skid:skid_setForPression"
//#include "hd:Users:racine:Desktop:Confi nov 20:prgm_Igor:Skid:skid_setForTemperature"
//#include "hd:Users:racine:Desktop:Confi nov 20:prgm_Igor:Skid:skid_setForFlux"


D�pendances fichiers (Soleil)
#include "D:Prgms_Igor:Skid:skid_IHM"
#include "D:Prgms_Igor:Skid:skid_Variables"
#include "D:Prgms_Igor:Skid:skid_Utilities"

#include "D:Prgms_Igor:Skid:skid_setForPression"
#include "D:Prgms_Igor:Skid:skid_setForTemperature"
#include "D:Prgms_Igor:Skid:skid_setForFlux"



function skid_Init()

	variable df_Exists = dataFolderExists("root:SKID")
	
	if (df_Exists == 0)		// 0 : n'existe pas
	
		// Creation Folders programme
		NewDataFolder root:SKID
		NewDataFolder root:SKID:Variables
		
		
		// Chargement fichier remplissage des IHM
		string previous_df = getdatafolder(1)
		setDataFolder root:SKID:Variables

			LoadWave /A/W/O/J/K=2/Q "D:PRGMs_Igor:Skid:skidPathNfile.txt"
		setDataFolder previous_df		
			
		// Chargement variables programme
		skid_variables()
	
	endif
	
	// Affichage panneau de contr�le et graphes (P, T�, Vanne)
	skid_IHMpressions()
	skid_IHMtemperatures()
	skid_IHMflux()
	skid_controlPanel()
	
	return 0
end













